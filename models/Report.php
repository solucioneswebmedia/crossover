<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "reports".
 *
 * @property integer $id
 * @property integer $date
 * @property integer $report_status
 * @property integer $patient_id
 * @property integer $operator_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'report_status', 'patient_id', 'operator_id',], 'required'],
            [['date', 'report_status', 'patient_id', 'operator_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Report #',
            'date' => 'Date Taken',
            'report_status' => 'Report Status',
            'patient_id' => 'Patient ID',
            'operator_id' => 'Operator ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getOperator()
    {
        return $this->hasOne(User::className(),['id'=>'operator_id']);
    }

    public function getPatient()
    {
        return $this->hasOne(User::className(),['id'=>'patient_id']);
    }
    public function getStatus()
    {
        switch($this->report_status)
        {
            case 1: return "New";
                      break;
            case 2: return "Processing";
                      break;
            case 3: return "Completed";
                      break;
        }
    }

    public function getDatetime()
    {
        return date('m-d-Y',$this->date);
    }

    public function setDatetime($dateText)
    {
        if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $dateText, $Matches) ){
                $this->date=strtotime($Matches[3]."-".$Matches[1]."-".$Matches[2]);
            }
    }

    public function beforeSave($insert){
        //only when not is new record
        if ($this->isNewRecord)
            $this->created_at = time();
        //set updated_at timestamp
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }

    public function initTests($data)
    {
        $this->setDatetime($data['Report']['datetime']);
        $this->save();
        if (isset($data['ProfileTests']['profile_id']))
        {
        $profiles=$data['ProfileTests']['profile_id'];
            foreach ($profiles as $profile_id) {
                //get profile object and create a result record for every test on that profile
                $profile=Profile::findOne($profile_id);
                if ($profile)
                {
                    foreach ($profile->tests as $test)
                    {
                        $reportResult= new ReportResult;
                        $reportResult->value="0";
                        $reportResult->test_id=$test->test_id;
                        $reportResult->profile_id=$profile->id;
                        $reportResult->user_id=$this->operator_id;
                        $reportResult->report_id=$this->id;
                        $reportResult->save();
                    }
                }
            }
        }
        if (isset($data['SingleTests']['test_id']))
        {
            $tests=$data['SingleTests']['test_id'];
            foreach ($tests as $test_id) {
                //get profile object and create a result record for every test on that profile
                $test=Test::findOne($test_id);
                if ($test)
                {
                        $reportResult= new ReportResult;
                        $reportResult->value="0";
                        $reportResult->test_id=$test->id;
                        $reportResult->profile_id=0;
                        $reportResult->user_id=$this->operator_id;
                        $reportResult->report_id=$this->id;
                        $reportResult->save();
                }
        }
        }
    }

    public function getTestResultsDataProvider()
    {
        $query = ReportResult::find()->andFilterWhere(['=','report_id',$this->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
    public function getTestResultsQuery()
    {
        $query = ReportResult::find()->andFilterWhere(['=','report_id',$this->id]);
        return $query;
    }

}
