<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile_tests".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $test_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ProfileTest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_tests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'test_id', 'created_at', 'updated_at'], 'required'],
            [['profile_id', 'test_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'test_id' => 'Test ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTest()
    {
        return $this->hasOne(Test::className(),['id'=>'test_id']);
    }

    public function getProfile()
    {
        return $this->hasOne(User::className(),['id'=>'profile_id']);
    }
}
