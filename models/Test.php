<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tests".
 *
 * @property integer $id
 * @property string $name
 * @property string $lower_limit
 * @property string $upper_limit
 * @property string $units
 * @property integer $results_type
 * @property integer $created_at
 * @property integer $updated_at
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'lower_limit', 'upper_limit', 'units'], 'required'],
            [['results_type', 'created_at', 'updated_at'], 'integer'],
            [['name', 'lower_limit', 'upper_limit', 'units'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lower_limit' => 'Lower Limit',
            'upper_limit' => 'Upper Limit',
            'units' => 'Units',
            'results_type' => 'Results Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert){
        //only when not is new record
        if ($this->isNewRecord)
            $this->created_at = time();
        //set updated_at timestamp
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }
}
