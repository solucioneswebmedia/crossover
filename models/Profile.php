<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property integer $id
 * @property string $description
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','active'], 'required'],
            [['active', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTests()
    {
        return $this->hasMany(ProfileTest::className(),['profile_id'=>'id']);
    }

    public function syncTests($postdata)
    {
        $newProfileTests=$postdata['ProfileTest']['test_id'];
        $oldProfileTests=ProfileTest::find()->select(['test_id'])->where(['profile_id'=>$this->id])->column();
        //get the new ones
        $newOnes=array_diff($newProfileTests, $oldProfileTests);
        foreach ($newOnes as $test) {
            $pt=new ProfileTest();
            $pt->profile_id=$this->id;
            $pt->test_id=$test;
            $pt->created_at=time();
            $pt->updated_at=time();
            $pt->save();
        }
        // now delete the old non used
        $oldOnes=array_diff($oldProfileTests,$newProfileTests);
        ProfileTest::deleteAll(['profile_id'=>$this->id,'test_id'=>$oldOnes]);
        
        return $this->hasMany(ProfileTest::className(),['profile_id'=>'id']);
    }

    public function getTestsArray()
    {
        $return=[];
        foreach ($this->tests as $test) {
            $return[]=$test->test_id;
        }
        return $return;
    }

    public function beforeSave($insert){
        //only when not is new record
        if ($this->isNewRecord)
            $this->created_at = time();
        //set updated_at timestamp
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }
}
