<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_results".
 *
 * @property integer $id
 * @property string $value
 * @property integer $test_id
 * @property integer $report_id
 * @property integer $profile_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class ReportResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'test_id', 'report_id', 'user_id'], 'required'],
            [['test_id', 'report_id', 'profile_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['value'], 'validateValue']
        ];
    }

    public function validateValue($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->test->results_type==1 &&  !@settype( $this->value , 'float' ))
                $this->addError($attribute, 'Incorrect data.');
            if ($this->test->results_type==2 &&  !@settype( $this->value , 'string' ))
                $this->addError($attribute, 'Incorrect data.');

        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'test_id' => 'Test ID',
            'report_id' => 'Report ID',
            'profile_id' => 'Profile ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert){
        //only when not is new record
        if ($this->isNewRecord)
            $this->created_at = time();
        //set updated_at timestamp
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }

    public function getTest()
    {
        return $this->hasOne(Test::className(),['id'=>'test_id']);
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(),['id'=>'profile_id']);
    }
    public function getReport()
    {
        return $this->hasOne(Report::className(),['id'=>'report_id']);
    }
}
