<?php

namespace app\models;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\base\Security;
use yii\web\IdentityInterface;


use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $full_name
 * @property string $email
 * @property string $birth_date
 * @property integer $sex
 * @property string $mobile_phone
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'full_name', 'email', 'birth_date', 'sex', 'mobile_phone'], 'required'],
            [['birth_date'], 'safe'],
            [['sex', 'active', 'created_at', 'updated_at'], 'integer'],
            [['username', 'full_name', 'email', 'mobile_phone'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 512, "min"=>8],
            [['username','email'], 'unique']
        ];
    }

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }


   public function beforeSave($insert){
        //only when not is new record
        if ($this->isNewRecord)
            $this->created_at = time();
        //if password change store MD5 no plain text
        if ($this->isAttributeChanged('password'))
            $this->password=md5($this->password);
        //setups dates for store them in MySQL
        if (isset($this->birth_date) ){  
            if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->birth_date, $Matches) ){
                $this->birth_date=$Matches[3]."-".$Matches[1]."-".$Matches[2];
            }
        }
        //set updated_at timestamp
        $this->updated_at = time();
        return parent::beforeSave($insert);
    }

    public function getUnformattedBirthDate()
    {
        $ret=NULL;
       if (isset($this->birth_date) ){  
            if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->birth_date, $Matches) ){
                $ret=$Matches[3]."-".$Matches[1]."-".$Matches[2];
            }
        } 
        return $ret;
    }
    
    public function afterFind()
    {
        if(preg_match('#(\d{4})-(\d{2})-(\d{2})#', $this->birth_date, $Matches) ){
                $this->birth_date=$Matches[2]."-".$Matches[3]."-".$Matches[1];
            }
        return parent::afterFind();
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'birth_date' => 'Birth Date',
            'sex' => 'Sex',
            'mobile_phone' => 'Mobile Phone',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getRole()
    {
        return $this->hasOne(UserRole::className(),['user_id'=>'id']);
    }

    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
/* modified */
    public static function findIdentityByAccessToken($token, $type = null)
    {
          throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
 
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $parts = explode('_', $token);
        $expire=(int)current($parts);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return self::findOne([
            'reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        throw new NotSupportedException('"getAuthKey" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
       throw new NotSupportedException('"getAuthKey" is not implemented.');
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }


    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->reset_token = strtotime("+1 day").'_'.utf8_encode(Yii::$app->security->generateRandomString()) . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->reset_token = null;
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    public function isAdmin()
    {
        if ($this->role->role_id==1)
            return true;
        return false;
    }

    public function isOperator()
    {
        if ($this->role->role_id==2)
            return true;
        return false;
    }

    public function isPatient()
    {
        if ($this->role->role_id==3)
            return true;
        return false;
    }
    public function loadPatient($data)
    {
        if ($this->isNewRecord)
        {
            //set the username and password
            $this->username="p_".Yii::$app->security->generateRandomString(10);
            $this->password=Yii::$app->security->generateRandomString(8);
            $this->reset_token=$this->password;
        }
        return $this->load($data);
    }
}
