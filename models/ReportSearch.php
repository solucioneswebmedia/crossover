<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 */
class ReportSearch extends Report
{
    private $operatorname;
    private $patientname;
    private $statusname;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'date', 'report_status', 'patient_id', 'operator_id', 'created_at', 'updated_at'], 'integer'],
            [['operatorname','patientname','statusname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find()->joinWith('operator o',false,'INNER JOIN')->joinWith('patient p',false,'INNER JOIN');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'report_status' => $this->report_status,
            'patient_id' => $this->patient_id,
            'operator_id' => $this->operator_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        if (strlen(trim($this->operatorname))>0)
            $query->andFilterWhere(['like', 'o.full_name',$this->operatorname ]);
        if (strlen(trim($this->patientname))>0)
            $query->andFilterWhere(['like', 'p.full_name',$this->patientname ]);
        if ($this->getStatusNameKey()>=0)
            $query->andFilterWhere(['=', 'report_status',$this->getStatusNameKey() ]);
        return $dataProvider;
    }

    public function getOperatorname()
    {
        return $this->operatorname;
    }
    public function setOperatorname($value)
    {
        $this->operatorname=$value;
    }

    public function getPatientname()
    {
        return $this->patientname;
    }
    public function setPatientname($value)
    {
        $this->patientname=$value;
    }
    public function getStatusname()
    {
        return $this->statusname;
    }
    public function setStatusname($value)
    {
        $this->statusname=$value;
    }
    public function getStatusNameKey()
    {
        if (strlen($this->statusname)<=0)
            return -1;
        switch(strtolower($this->statusname[0]))
        {
            case 'n': return 1;
                      break;
            case 'p': return 2;
                      break;
            case 'c': return 3;
                      break;
        }
        return -1;
    }
}
