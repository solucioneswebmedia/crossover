<?php

namespace app\modules\patients;

class Patients extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\patients\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
