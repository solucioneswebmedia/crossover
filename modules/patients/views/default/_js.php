<?php 
use yii\web\View;
$this->registerJs("

var custom_buttons_base_url='".Yii::$app->urlManager->createAbsoluteUrl("/")."'
//when user clicks update button on GridView
$('.custom-send-button').click(this,function(e){
	var model=this.dataset.model;
    bootbox.confirm('Do you want to send this report to your registered email?', function(result) {                
  		if (result) {                                             
    		$.ajax(custom_buttons_base_url+'patients/my-reports/send', {
                    data: {
                        'id':model,
                    },
                    dataType: 'json'
                }).done(function(data){
                	if (data.result=='ok')
                	{
                		//reload the page
                		bootbox.alert('Test Report sent to '+data.email, function() {});
                	}
                	else
                	{
                		bootbox.alert(data.error_msg,function(){});
                	}
                });
  		}
});
});", View::POS_READY);