<?php
use yii\helpers\Html;
use app\models\PatientSearch;
use app\models\Profile;
use app\models\Test;
use yii\grid\GridView;

$this->title = 'LISA - My Reports';
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['/patients']];
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-dashboard"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    [
                        'attribute' => 'datetime',
                        'value' => 'datetime',
                        'label'=>'Date'
                    ],
                    [
                        'attribute' => 'operatorname',
                        'value' => 'operator.full_name',
                        'label'=>'Operator'
                    ],
                    [
                        'attribute' => 'patientname',
                        'value' => 'patient.full_name',
                        'label'=>'Patient'
                    ],
                    // 'created_at',
                    // 'updated_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {send} {export}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', "/patients/my-reports/view?id=".$model->id, [
                                            'title' => 'view this report','class'=>'custom-view-button',
                                            'data-model'=>$model->id]);
                            },
                            'send' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-envelope"></span>', "#", [
                                            'title' => 'send to your email address','class'=>'custom-send-button',
                                            'data-model'=>$model->id]);
                            },
                            'export' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-download"></span>', "/patients/my-reports/export?id=".$model->id, [
                                            'title' => 'download as PDF','class'=>'custom-export-button',
                                            'data-model'=>$model->id,'target'=>'_blank']);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div><!-- /.row -->
        <?= $this->render('_js') ?>
    </div>
</div>
