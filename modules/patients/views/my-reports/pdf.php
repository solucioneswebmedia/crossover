<?php
/* @var $this yii\web\View */
?>
<div class="col-md-12" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-bar-chart"></span> Test Results Report</h3>
	    </div>
	    <div class="box-body">
		    <?= $this->render('_form_pdf', [
		        'model' => $model,
		    ]) ?>

		</div>
	</div>
</div>