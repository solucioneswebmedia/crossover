<?php
/* @var $this yii\web\View */
$this->title = 'View Test Report '.$model->id." - ".$model->patient->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['/patients']];
$this->params['breadcrumbs'][] = $model->patient->full_name;
?>
<div class="col-md-10" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-bar-chart"></span> Test Results Report</h3>
	    </div>
	    <div class="box-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
	</div>
</div>