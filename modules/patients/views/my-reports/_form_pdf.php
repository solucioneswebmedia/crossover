<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\PatientSearch;
use app\models\Profile;
use app\models\Test;
use kartik\daterange\DateRangePicker;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-form">
    <div class="row">
        <div class="form-group col-md-6">
                <label>Patient</label>
                <div class="form-group">
                    <?= $model->patient->full_name ?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Taken on</label>
                <div class="form-group">
                    <?= $model->getDatetime() ?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Taked by</label>
                <div class="form-group">
                    <?= $model->operator->full_name ?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Birth Date</label>
                <div class="form-group">
                    <?= $model->patient->birth_date ?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Sex</label>
                <div class="form-group">
                    <?= $model->patient->sex==1?'Male':'Female' ?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Mobile Phone</label>
                <div class="form-group">
                    <i class='fa fa-mobile'></i><?= $model->patient->mobile_phone?>
                </div>
        </div>
        <div class="form-group col-md-3">
                <label>Email</label>
                <div class="form-group">
                    <i class='fa fa-envelope'></i><?= $model->patient->email ?>
                </div>
        </div>
      
         <div class="col-md-12" style="margin:auto!important;">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><span class="fa fa-bar-chart"></span> Laboratory Report Results</h3>
                </div>
                <div class="box-body">
                <div class="profile-tests-view">
                    <?= GridView::widget([
                            'dataProvider' => $model->getTestResultsDataProvider(),
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'profile.description',
                                'test.name',
                                'value',
                                'test.lower_limit',
                                'test.upper_limit',
                                'test.units',
                            ]                                
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<p>This Results are confidencial and protected by law. Please delete this message from your inbox if you are not the right recipient of this information. Be carefull with the environment, do not print this results unless if strictly necessary.</p>
<hr/>
<p>Remenber you can see all your test results online on <?= Yii::$app->urlManager->createAbsoluteUrl("/") ?></p>
<P> <strong>User:</strong> <?= $model->patient->full_name ?> - <strong>Pass code:</strong> <?= $model->patient->reset_token ?></p> 
