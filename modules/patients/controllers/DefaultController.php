<?php

namespace app\modules\patients\controllers;

use yii\web\Controller;
use app\models\Report;
use yii\data\ActiveDataProvider;
use \yii\filters\AccessControl;
use \yii\filters\VerbFilter;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
    	$query = Report::find()->joinWith('operator o',false,'INNER JOIN')->joinWith('patient p',false,'INNER JOIN');
    	$query->andFilterWhere(['like', 'p.id',\Yii::$app->user->identity->id ]);
    	$query->andFilterWhere(['=', 'reports.report_status',3 ]);
    	$dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        return $this->render('index',['dataProvider'=>$dataProvider]);
    }
}
