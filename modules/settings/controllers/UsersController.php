<?php

namespace app\modules\settings\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\UserRole;
use app\models\ChangePasswordForm;
use app\models\SendResetPasswordForm;
use app\models\Role;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \yii\filters\VerbFilter;
use \yii\filters\AccessControl;
use \yii\db\Query;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','view','change'],
                'rules' => [
                    [
                        'actions' => ['index','create','update','view','change'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $modelRole = new UserRole();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelRole->load(Yii::$app->request->post());
            $modelRole->user_id=$model->id;
            if ($modelRole->save())
                return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelRole' => $modelRole,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelRole=$model->role;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $modelRole->load(Yii::$app->request->post());
            $modelRole->user_id=$model->id;
            if ($modelRole->save())
                return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelRole'=>$modelRole
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionUserList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, full_name AS text')
                ->from('users')
                ->where(['like', 'full_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->full_name];
        }
        return $out;
    }

    public function actionChange()
    {
        $model = $this->findModel(Yii::$app->user->identity->getId());
        $cpForm=new ChangePasswordForm();
        $cpForm->user_id=$model->id;
        if ($cpForm->load(Yii::$app->request->post()) && $cpForm->validate()) {
            $model->password=$cpForm->password;
            if ($model->save())
                return $this->redirect(['/settings']);
        } else {
            return $this->render('change', [
                'model' => $cpForm,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
