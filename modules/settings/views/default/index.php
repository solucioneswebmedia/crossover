<?php
use yii\helpers\Html;
use app\models\User;
use app\models\Profile;
use app\models\Test;

$this->title = 'LISA - Settings Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-cogs"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <?php
                if (Yii::$app->user->identity->isAdmin())
                {
            ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <?php echo count(User::find()->where(['active'=>1])->all()) ?>
                        </h3>
                        <p>
                            <i class="fa fa-users"></i> Active Users
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="settings/users" class="small-box-footer">
                        Manage Users <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <?php echo count(Profile::find()->where(['active'=>1])->all()) ?>
                        </h3>
                        <p>
                            <i class="fa fa-eyedropper"></i> Active Profiles
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="settings/profiles" class="small-box-footer">
                        Manage Profiles <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            <?php echo count(Test::find()->all()) ?>
                        </h3>
                        <p>
                            <i class="fa fa-bar-chart"></i> Available Tests
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="settings/tests" class="small-box-footer">
                         Manage Tests<i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <?php } ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-envelope"></i>
                        </h3>
                        <p>
                            Change your Password
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="settings/users/change" class="small-box-footer">
                        Click to change <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
        </div><!-- /.row -->
    </div>
</div>
