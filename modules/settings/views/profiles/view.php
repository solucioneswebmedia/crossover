<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => 'Test', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-eyedropper"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="profile-view">
            <p class="text-center">
                <?= Html::a('<span class="fa fa-edit"></span> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<span class="fa fa-trash"></span> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'description',
                    array('attribute'=>'active',
                        'value'=>($model->active==1)?'Yes':'No'
                    ),
                    array('attribute'=>'created_at',
                        'value'=>date('m-d-Y H:i:s',$model->created_at)
                    ),
                    array('attribute'=>'updated_at',
                        'value'=>date('m-d-Y H:i:s',$model->updated_at)
                    )
                ],
            ]) ?>

        </div>
    </div>
</div>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-eyedropper"></span> <?= Html::encode($this->title) ?> - Linked Tests</h3>
    </div>
    <div class="box-body">
        <div class="profile-tests-view">
        <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'test.name',
                    
                ],
            ]); ?>
        </div>
    </div>
</div>
