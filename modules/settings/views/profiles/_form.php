<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2; // or kartik\select2\Select2
use yii\web\JsExpression;
use app\models\Test;
$url = \yii\helpers\Url::to(['/settings/tests/test-list']);

/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
        <div class="form-group col-md-6">
            <label for="Test[name]">Profile Description</label>
            <?= $form->field($model, 'description')->textInput(['maxlength' => true])->label(false) ?>
        </div>
        <div class="form-group col-md-6">
            <label>Active</label>
            <?= $form->field($model, 'active')->dropdownList(
                    ['1'=>'Yes','2'=>'No'],
                    ['prompt'=>'choose one']
                )->label(false);
            ?>
        </div>
        <div class="form-group col-md-12">
            <label>Linked Laboratory Tests</label>
	        <?= Select2::widget([
			    'name' => 'ProfileTest[test_id]',
			    'value' => $model->getTestsArray(), // initial value
			    'data' => Test::find()->select(['name', 'id'])->indexBy('id')->column(),
			    'options' => ['placeholder' => 'Add some tests to this profile...', 'multiple' => true],
			    'pluginOptions' => [
			        'tags' => true,
			        'maximumInputLength' => 10
			    ],
			]); ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <p class="text-center">
            <?= Html::submitButton($model->isNewRecord ? '<span class="fa fa-save"></span> Create' : '<span class="fa fa-save"></span> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
