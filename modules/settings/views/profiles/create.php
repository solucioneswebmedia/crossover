<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = 'Create Profile';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => 'Tests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-10" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-eyedropper"></span> Fill New Profile Information</h3>
	    </div>
	    <div class="box-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
	</div>
</div>
