<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Test */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="Test[name]">Test Name</label>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>

        <div class="form-group col-md-6">
            <label>Results Type</label>
            <?= $form->field($model, 'results_type')->dropdownList(
                    ['1'=>'Numeric','2'=>'Alphanumeric'],
                    ['prompt'=>'choose one']
                )->label(false);
            ?>
        </div><!-- /.form group -->

        <div class="form-group col-md-6">
            <label for="Tests[lower_limit]">Lower Limit</label>
            <?= $form->field($model, 'lower_limit')->textInput(['maxlength' => true])->label(false) ?>
        </div>

        <div class="form-group col-md-6">
            <label for="Tests[upper_limit]">Upper Limit</label>
            <?= $form->field($model, 'upper_limit')->textInput(['maxlength' => true])->label(false) ?>
        </div>

        <div class="form-group col-md-6">
            <label for="Tests[units]">Units</label>
            <?= $form->field($model, 'units')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>

    <?php $this->registerJs("
            //when user sets results_type
            $('#test-results_type').change(this,function(e){
                var rtype=$(this).val();
                if (rtype==2)
                {
                    $('#test-lower_limit').val('N/A').prop('readonly', true);
                    $('#test-upper_limit').val('N/A').prop('readonly', true);
                    $('#test-units').val('N/A').prop('readonly', true);
                }
                else
                {
                    $('#test-lower_limit').val('').prop('readonly', false);
                    $('#test-upper_limit').val('').prop('readonly', false);
                    $('#test-units').val('').prop('readonly', false);
                }

            });

            if ($('#test-results_type').val()==2)
            {
                $('#test-lower_limit').val('N/A').prop('readonly', true);
                $('#test-upper_limit').val('N/A').prop('readonly', true);
                $('#test-units').val('N/A').prop('readonly', true);
            }
    ", View::POS_READY);?>
    <div class="row">
        <div class="form-group">
            <p class="text-center">
            <?= Html::submitButton($model->isNewRecord ? '<span class="fa fa-save"></span> Create' : '<span class="fa fa-save"></span> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
