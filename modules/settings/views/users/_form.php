<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use app\models\UserRole;
use app\models\Role;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="form-group col-md-6">
        <label for="User[username]">Username</label>
        <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group col-md-6">
        <label for="User[full_name]">Full Name</label>
        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true])->label(false) ?>
    </div>
    <?php if ($model->isNewRecord){ ?>
    <div class="form-group col-md-6">
        <label for="User[password]">Password</label>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true])->label(false) ?>
    </div>
    <?php } ?>
    <div class="form-group col-md-6">
        <label for="User[email]">Email Address</label>
        <?= $form->field($model, 'email')->input('email')->label(false) ?>
    </div> 

    <div class="form-group col-md-6">
        <label>Birth Date</label>
            <?= $form->field($model, 'birth_date')->widget(DateRangePicker::classname(), [
                'useWithAddon'=>false, 'pluginOptions'=>['singleDatePicker'=>true,'showDropdowns'=>true,'locale'=>['format'=>'m-d-Y']],
                'convertFormat'=>true,

            ])->label(false); ?>
        
    </div><!-- /.form group -->

    <div class="form-group col-md-6">
        <label>Sex</label>
        <?= $form->field($model, 'sex')->dropdownList(
                ['1'=>'Male','2'=>'Female'],
                ['prompt'=>'Select Sex']
            )->label(false);
        ?>
    </div><!-- /.form group -->

    <div class="form-group col-md-6">
        <label>Phone Number</label>
        <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true])->label(false) ?>
    </div>

     <div class="form-group col-md-6">
        <label>Active</label>
        <?= $form->field($model, 'active')->dropdownList(
                ['1'=>'Yes','2'=>'No'],
                ['prompt'=>'choose one']
            )->label(false);
        ?>
    </div><!-- /.form group -->

    <div class="form-group col-md-6">
        <label>User Role</label>
        <?= $form->field($modelRole, 'role_id')->dropdownList(
                Role::find()->select(['description', 'id'])->indexBy('id')->column(),
                ['prompt'=>'choose one']
            )->label(false);
        ?>
    </div><!-- /.form group -->

    </div>
    <div class="row">
        <div class="form-group">
        <p class="text-center">
            <?= Html::submitButton($model->isNewRecord ? '<span class="fa fa-save"></span> Create' : '<span class="fa fa-save"></span> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
        </p>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
