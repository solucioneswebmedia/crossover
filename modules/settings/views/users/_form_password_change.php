<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChangePasswordForm*/
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <?= $form->field($model, 'user_id')->hiddenInput([])->label(false);?>

    <div class="form-group col-md-6">
        <label for="ChangePasswordForm[old_password]">Current Password</label>
        <?= $form->field($model, 'old_password')->passwordInput(['maxlength' => true])->label(false) ?>
    </div>

    <div class="form-group col-md-6">
        <label for="ChangePasswordForm[password]">New Password</label>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true])->label(false) ?>
    </div>
    
    <div class="form-group col-md-6">
        <label for="ChangePasswordForm[password]">New Password Confirmation</label>
        <?= $form->field($model, 'confirmation')->passwordInput(['maxlength' => true])->label(false) ?>
    </div>

    </div>
    <div class="row">
        <div class="form-group">
        <p class="text-center">
            <?= Html::submitButton('<span class="fa fa-save"></span> Change Password' , ['class' => 'btn btn-success']) ?>
            <?= Html::a('<span class="fa fa-stop"></span> Cancel', ['/settings'], ['class' => 'btn btn-danger']) ?>
        </p>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
