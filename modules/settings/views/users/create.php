<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $modelRole app\models\UserRole */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-10" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-user"></span> Fill New User Information</h3>
	    </div>
	    <div class="box-body">
	        <?= $this->render('_form', [
			        'model' => $model,
			        'modelRole'=>$modelRole
			]) ?>
	    </div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
