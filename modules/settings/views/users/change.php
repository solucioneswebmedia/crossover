<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ChangePasswordForm */

$this->title = 'Change your password';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-10" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-user"></span> Change your password</h3>
	    </div>
	    <div class="box-body">
	        <?= $this->render('_form_password_change', [
			        'model' => $model,
			]) ?>
	    </div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
