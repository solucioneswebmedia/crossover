<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Users';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-users"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="user-index">
            <p>
                <?= Html::a('<span class="fa fa-plus"></span> Create User', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<span class="fa fa-ban"></span> Clear Filters', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'username',
                    'full_name',
                    'email:email',
                    [
                        'attribute' => 'rolename',
                        'value' => 'role.role.description',
                        'label'=>'Role Type'
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>