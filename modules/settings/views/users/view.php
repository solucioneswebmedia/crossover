<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-user"></span> <?= Html::encode($this->title) ?> - (<?= $model->role->role->description ?>)</h3>
    </div>
    <div class="box-body">
        <div class="user-view">
            <p class="text-center">
                <?= Html::a('<span class="fa fa-edit"></span> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<span class="fa fa-trash"></span> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'username',
                    'full_name',
                    'email:email',
                    array('attribute'=>'birth_date',
                        'value'=>date('m-d-Y',strtotime($model->getUnformattedBirthDate()))
                    ),
                    array('attribute'=>'sex',
                        'value'=>($model->sex==1)?'Male':'Female'
                    ),
                    'mobile_phone',
                    array('attribute'=>'active',
                        'value'=>($model->active==1)?'Yes':'No'
                    ),
                    array('attribute'=>'created_at',
                        'value'=>date('m-d-Y H:i:s',$model->created_at)
                    ),
                    array('attribute'=>'updated_at',
                        'value'=>date('m-d-Y H:i:s',$model->updated_at)
                    )
                ],
            ]) ?>

        </div>
    </div>
</div>
