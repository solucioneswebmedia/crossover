<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $modelRole app\models\UserRole */

$this->title = 'Update User: ' . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['/settings']];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-user"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
		<div class="user-update">
		    <?= $this->render('_form', [
		        'model' => $model,
		        'modelRole' => $modelRole,
		    ]) ?>

		</div>
	</div>
</div>
