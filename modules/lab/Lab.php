<?php

namespace app\modules\lab;

class Lab extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\lab\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
