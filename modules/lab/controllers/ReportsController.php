<?php

namespace app\modules\lab\controllers;

use Yii;
use app\models\Report;
use app\models\ReportResult;
use app\models\ReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\filters\AccessControl;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','view'],
                'rules' => [
                    [
                        'actions' => ['index','create','update','view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Report();
        $model->date=time();
        $model->report_status=1;
        $model->operator_id=Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->initTests(Yii::$app->request->post());
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $data=Yii::$app->request->post();
        if (isset($data['Report']['datetime']))
            $model->setDatetime($data['Report']['datetime']);
        if ($model->load($data) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteResult($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['result' =>"ok",'error_msg'=>''];
        $result=ReportResult::findOne($id);
        if (is_null($result) || !$result->delete())
        {
            $out['result']='error';
            $out['error_msg']='Can not delete this result object please refresh your web browser page';
        }
        return $out;
    }

    public function actionUpdateResult($id,$value)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['result' =>"ok",'error_msg'=>''];
        $result=ReportResult::findOne($id);
        if (is_null($result))
        {
            $out['result']='error';
            $out['error_msg']='Object not found';
        }
        else
        {
            $result->value=$value;
            if (!$result->save())
            {
                $out['result']='error';
                $out['error_msg']='Can not update this result, please try again and check you are entering a valid value for this test';
            }

        }
        return $out;
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
