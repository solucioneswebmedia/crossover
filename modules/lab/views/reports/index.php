<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Tests Reports';
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/lab']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-bar-chart"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="user-index">
            <p>
                <?= Html::a('<span class="fa fa-plus"></span> Create Reports', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<span class="fa fa-ban"></span> Clear Filters', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    [
                        'attribute' => 'datetime',
                        'value' => 'datetime',
                        'label'=>'Date'
                    ],
                    [
                        'attribute' => 'statusname',
                        'value' => 'status',
                        'label'=>'Status'
                    ],
                    [
                        'attribute' => 'operatorname',
                        'value' => 'operator.full_name',
                        'label'=>'Operator'
                    ],
                    [
                        'attribute' => 'patientname',
                        'value' => 'patient.full_name',
                        'label'=>'Patient'
                    ],
                    // 'created_at',
                    // 'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>
