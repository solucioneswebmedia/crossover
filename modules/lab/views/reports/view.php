<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

$this->title = "Test Results Report ".$model->id." for ".$model->patient->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/lab']];
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-bar-chart"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="user-view">
            <p class="text-center">
                <?= Html::a('<span class="fa fa-edit"></span> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<span class="fa fa-trash"></span> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    ['attribute'=>'date',
                        'value'=>date('m-d-Y',$model->date)
                    ],
                    'status',
                    ['attribute'=>'patient.full_name',
                        'label'=>'Patient'
                    ],
                    ['attribute'=>'operator.full_name',
                        'label'=>'Operator'
                    ],
                    ['attribute'=>'created_at',
                        'value'=>date('m-d-Y',$model->created_at)
                    ],
                    ['attribute'=>'updated_at',
                        'value'=>date('m-d-Y',$model->updated_at)
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
