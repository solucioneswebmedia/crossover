<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\PatientSearch;
use app\models\Profile;
use app\models\Test;
use kartik\daterange\DateRangePicker;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="form-group col-md-6">
        <?= $form->field($model, 'patient_id')->widget(Select2::classname(), [
            'initValueText' => '', // set the initial display text
            'options' => ['placeholder' => 'choose a patient...'],
            'data' => PatientSearch::find()->select(['full_name', 'users.id as id'])->joinWith('role',false,'INNER JOIN')->where(['=','user_roles.role_id',3])->indexBy('id')->column(),
            'value'=>[],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label('Patient') ?>
        </div>
        <?php if ($model->isNewRecord)
        { ?>
            <div class="form-group col-md-6">
                <label>Taken on Date</label>
                    <?= $form->field($model, 'datetime')->widget(DateRangePicker::classname(), [
                        'useWithAddon'=>false, 'pluginOptions'=>['singleDatePicker'=>true,'showDropdowns'=>true,'locale'=>['format'=>'m-d-Y']],
                        'convertFormat'=>true,

                    ])->label(false); ?>
                
                </div>
            <?= $form->field($model, 'report_status')->hiddenInput([])->label(false);?>
            <div class="form-group col-md-6">
                <label>What Tests Profiles were Taken?</label>
                <?= Select2::widget([
                    'name' => 'ProfileTests[profile_id]',
                    'value' => [], // initial value
                    'data' => Profile::find()->select(['description', 'id'])->where(['=','active',1])->indexBy('id')->column(),
                    'options' => ['placeholder' => 'Choose tests profiles...','multiple'=>true],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10
                    ],
                ]); ?>
            </div>
            <div class="form-group col-md-6">
                <label>What Single Tests were Taken?</label>
                <?= Select2::widget([
                    'name' => 'SingleTests[test_id]',
                    'value' => [], // initial value
                    'data' => Test::find()->select(['name', 'id'])->indexBy('id')->column(),
                    'options' => ['placeholder' => 'Choose single tests...','multiple'=>true],
                    'pluginOptions' => [
                        'tags' => true,
                        'maximumInputLength' => 10
                    ],
                ]); ?>
            </div>
        <?php } else{ ?>
            <div class="form-group col-md-3">
            <label>Taken on Date</label>
                <?= $form->field($model, 'datetime')->widget(DateRangePicker::classname(), [
                    'useWithAddon'=>false, 'pluginOptions'=>['singleDatePicker'=>true,'showDropdowns'=>true,'locale'=>['format'=>'m-d-Y']],
                    'convertFormat'=>true,

                ])->label(false); ?>
            
            </div>
             <div class="form-group col-md-3">
                <label>Report Status</label>
                <?= $form->field($model, 'report_status')->dropdownList(
                        ['1'=>'New','2'=>'Processing','3'=>'Completed'],
                        ['prompt'=>'choose one']
                    )->label(false);
                ?>
            </div>
             <div class="col-md-12" style="margin:auto!important;">
                <div class="box box-solid box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><span class="fa fa-bar-chart"></span> Laboratory Report Results</h3>
                    </div>
                    <div class="box-body">
                    <div class="profile-tests-view">
                        <?= GridView::widget([
                                'dataProvider' => $model->getTestResultsDataProvider(),
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'profile.description',
                                    'test.name',
                                    'value',
                                    'test.units',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{update} {delete}',
                                        'buttons' => [
                                            'update' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', "#", [
                                                            'title' => 'update this result','class'=>'custom-update-button',
                                                            'data-model'=>$model->id]);
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', "#", [
                                                            'title' => 'delete this result','class'=>'custom-delete-button',
                                                            'data-model'=>$model->id
                                                            ]);
                                            }
                                        ],
                                    ]
                                ]                                
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->render('_js',['model'=>$model]) ?>
        <?php } ?>
        <?= $form->field($model, 'operator_id')->hiddenInput([])->label(false);?>
        
    </div>

    <div class="row">
        <div class="form-group">
            <p class="text-center">
            <?= Html::submitButton($model->isNewRecord ? '<span class="fa fa-save"></span> Create' : '<span class="fa fa-save"></span> Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('<span class="fa fa-arrow-left"></span> Back', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
