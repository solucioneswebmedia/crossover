<?php 
use yii\web\View;
$this->registerJs("

var custom_buttons_base_url='".Yii::$app->urlManager->createAbsoluteUrl("/")."'
var custom_buttons_return_url='".Yii::$app->urlManager->createAbsoluteUrl(["/lab/reports/update",'id'=>$model->id])."'
//when user clicks update button on GridView
$('.custom-update-button').click(this,function(e){
	var model=this.dataset.model;
    bootbox.prompt('Enter new value for this result', function(result) {                
  		if (result === null) {                                             
    		console.log ('prompt dismissed')
  		} else {
    		$.ajax(custom_buttons_base_url+'lab/reports/update-result', {
                    data: {
                        'id':model,
                        'value':result
                    },
                    dataType: 'json'
                }).done(function(data){
                	if (data.result=='ok')
                	{
                		//reload the page
                		window.open(custom_buttons_return_url,'_self');
                	}
                	else
                	{
                		bootbox.alert(data.error_msg,function(){});
                	}
                });
  		}
});
});
//when user clicks delete button on GridView
$('.custom-delete-button').click(this,function(e){
    var model=this.dataset.model;
    bootbox.confirm('Are you sure to delete this?',function(result) {
  			if (result)
  			{
  				$.ajax(custom_buttons_base_url+'lab/reports/delete-result', {
                    data: {
                        'id':model,
                    },
                    dataType: 'json'
                }).done(function(data){
                	if (data.result=='ok')
                	{
                		//reload the page
                		window.open(custom_buttons_return_url,'_self');
                	}
                	else
                	{
                		bootbox.alert(data.error_msg,function(){});
                	}
                });
  			}
	}); 
});", View::POS_READY);