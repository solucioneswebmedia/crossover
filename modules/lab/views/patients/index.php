<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Patients';
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/lab']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-users"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="user-index">
            <p>
                <?= Html::a('<span class="fa fa-plus"></span> Create Patient', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<span class="fa fa-ban"></span> Clear Filters', ['index'], ['class' => 'btn btn-warning']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'full_name',
                    'email:email',
                    // 'birth_date',
                    //'sex',
                    'mobile_phone',
                    // 'active',
                    // 'created_at',
                    // 'updated_at',
                    // 'reset_token',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>
</div>
