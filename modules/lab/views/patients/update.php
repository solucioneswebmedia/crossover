<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update Patient: ' . ' ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/lab']];
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-user"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
		<div class="patient-update">
		    <?= $this->render('_form', [
		        'model' => $model,
		        'modelRole' => $modelRole,
		    ]) ?>

		</div>
	</div>
</div>