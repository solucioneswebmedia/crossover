<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Create Patient';
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/labs']];
$this->params['breadcrumbs'][] = ['label' => 'Patients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-10" style="margin:auto!important;">
	<!-- Primary box -->
	<div class="box box-solid box-primary">
	    <div class="box-header">
	        <h3 class="box-title"><span class="fa fa-user"></span> Fill New Patient Information</h3>
	    </div>
	    <div class="box-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		        'modelRole' => $modelRole
		    ]) ?>

		</div>
	</div>
</div>