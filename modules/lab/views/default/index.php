<?php
use yii\helpers\Html;
use app\models\PatientSearch;
use app\models\Profile;
use app\models\Test;

$this->title = 'LISA - Operators Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Lab', 'url' => ['/lab']];
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-eyedropper"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <?php
                if (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isOperator())
                {
            ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-users"></i>
                        </h3>
                        <p>
                            Patients
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="lab/patients" class="small-box-footer">
                        Manage Patients <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <i class="fa fa-eyedropper"></i>
                        </h3>
                        <p>
                            Test Reports
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="lab/reports" class="small-box-footer">
                        Manage Test Reports <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
            <?php } ?>
        </div><!-- /.row -->
    </div>
</div>
