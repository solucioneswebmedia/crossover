<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\controllers\MigrateController;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SetupController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex()
    {  	
        echo "Initializing LISA Setup Tool\n\n[LISA] Building Database Structure....\n";
        //run migrations
        $migration = new \yii\console\controllers\MigrateController('migrate',\Yii::$app);
     	$migration->run('up', ['migrationPath' => '@app/migrations/']);
     	echo "\n\n[LISA] Creating predefined Users & Roles....\n";
     	//create predefined users and roles
		\Yii::$app->db->createCommand()->insert('roles', [
		    'id' => 1,
		    'description' => "Admin",
		    'created_at' =>time(),
		    'updated_at' =>time()
		])->execute();
		\Yii::$app->db->createCommand()->insert('roles', [
		    'id' => 2,
		    'description' => "Operator",
		    'created_at' =>time(),
		    'updated_at' =>time()
		])->execute();
		\Yii::$app->db->createCommand()->insert('roles', [
		    'id' => 3,
		    'description' => "Patient",
		    'created_at' =>time(),
		    'updated_at' =>time()
		])->execute();
		\Yii::$app->db->createCommand()->insert('users', [
		    'id' => 1,
		    'username'  => "sysadmin",
		    'password'  => md5("sysadmin"),
		    'full_name' => "System Administrator",
		    'email' => "admin@admin.com",
		    'sex' => 1,
		    'birth_date' =>date('Y-m-d'),
		    'mobile_phone'=>"000-000-0000",
		    'active'=>1,
		    'created_at' =>time(),
		    'updated_at' =>time()
		])->execute();
		\Yii::$app->db->createCommand()->insert('user_roles', [
		    'id' => 1,
		    'user_id'  => 1,
		    'role_id'  => 1,
		    'created_at' =>time(),
		    'updated_at' =>time()
		])->execute();
		echo "\n\n[LISA] Done go to ".\Yii::getAlias('@lisa')."!!!!\n";

    }
}
