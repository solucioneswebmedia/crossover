<?php
// The controller action that will render the list
$url = \yii\helpers\Url::to(['/settings/users/user-list']);
 
// The widget
use kartik\select2\Select2; // or kartik\select2\Select2
use yii\web\JsExpression;
use app\models\User;
use yii\widgets\ActiveForm;
 
// Get the initial city description
$initialUser = empty($model->id) ? '' : User::findOne($model->id)->full_name;
 
?>


<body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Sign In</div>
            <?php $form = ActiveForm::begin(); ?>
                <div class="body bg-gray">
                    <div class="form-group">
                        <?= $form->field($model, 'id')->widget(Select2::classname(), [
                                'initValueText' => '', // set the initial display text
                                'options' => ['placeholder' => 'start typing your name and select from list...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(user) { return user.text; }'),
                                    'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                                ],
                            ]); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
                    <p><a class="btn btn-success btn-block" href="/"><i class="fa fa-home"></i> Go to Home Page</a></p>
                </div>
            <?php ActiveForm::end(); ?>

        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>
