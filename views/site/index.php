<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Laboratory Information Services Application - LISA';
?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><span class="fa fa-dashboard"></span> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">
        <div class="site-index">

            <div class="jumbotron">
                <h1>Welcome to LISA!</h1>
                <p class="lead"><img src="/pathology1.jpg"></p>
            </div>

            <div class="body-content">

                <div class="row">
                    <div class="col-lg-4">
                        <h2>Developed with Yii2</h2>

                        <p>I have developed software in real life projects with Yii, Yii2, Laravel, CakePHP. I think Frameworks lead us to achieve more complex goals in a project than work from scratch</p>
                    </div>
                    <div class="col-lg-4">
                        <h2>Who I am?</h2>

                        <p>My Name is Juan Scarton  and I'm currently applying with crossover for Software Engineer - PHP position.</p>

                    </div>
                    <div class="col-lg-4">
                        <h2>Do you like what I did with this assignment?</h2>

                        <p>Go ahead!! reach me on...</p> 

                        <p><i class="fa fa-envelope"></i> jscarton@gmail.com </p>
                        <p><i class="fa fa-whatsapp"></i>+57 319.451.7896</p>
                        <p><i class="fa fa-twitter"></i> @jscarton </p>
                        <p><i class="fa fa-facebook"></i>Juan Scarton</p>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
