<?php
use \app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use xj\bootbox\BootboxAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
//register with replace Yii.confirm
BootboxAsset::registerWithOverride($this);
?>

<header class="header">

<?= Html::a(Yii::$app->name, Yii::$app->homeUrl, ['class' => 'logo']) ?>

<nav class="navbar navbar-static-top" role="navigation">

<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>

<div class="navbar-right">

<ul class="nav navbar-nav">
<?php
if (Yii::$app->user->isGuest) {
    ?>
    <li class="footer">
        <?= Html::a('Login', ['/site/login']) ?>
    </li>
<?php
} else {
    ?>
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i>
            <span><?= @Yii::$app->user->identity->full_name ?> <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue">
                <?php if (@Yii::$app->user->identity->sex==1) { ?>
                    <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
                <?php } else { ?>
                    <img src="<?= $directoryAsset ?>/img/avatar2.png" class="img-circle" alt="User Image"/>
                <?php } ?>

                <p>
                    <?= @Yii::$app->user->identity->full_name?> - <?= @Yii::$app->user->identity->role->role->description?>
                </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
            <?php if (!Yii::$app->user->identity->isPatient())
            { ?>
                <div class="pull-left">
                    <a href="settings/users/change" class="btn btn-default btn-flat">Change Password</a>
                </div>
            <?php } ?>
                <div class="pull-right">
                    <?= Html::a(
                            'Sign out',
                            ['/site/logout'],
                            ['data-method' => 'post','class'=>'btn btn-default btn-flat']
                        ) ?>
                </div>
            </li>
        </ul>
    </li><?php
}
?>
<!-- User Account: style can be found in dropdown.less -->

</ul>
</div>
</nav>
</header>
