<?php
use yii\bootstrap\Nav;

?>
<aside class="left-side sidebar-offcanvas">

    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <?php if (@Yii::$app->user->identity->sex==1) { ?>
                        <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
                    <?php } else { ?>
                        <img src="<?= $directoryAsset ?>/img/avatar2.png" class="img-circle" alt="User Image"/>
                    <?php } ?>
                </div>
                <div class="pull-left info">
                    <p>Hello, <?= @Yii::$app->user->identity->full_name ?></p>
                    <a href="<?= $directoryAsset ?>/#">
                        <i class="fa fa-circle text-success"></i> Online
                    </a>
                </div>
            </div>
        <?php endif ?>

        <?=
        Nav::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => '<span class="fa fa-angle-down"></span><span class="text-info">LISA V1.0</span>',
                        'url' => '#'
                    ],
                    ['label' => '<span class="fa fa-dashboard"></span> Home', 'url' => ['/']],
                    ['label' => '<span class="fa fa-cogs"></span><span class="text-info"> Settings</span>', 'url' => ['/settings'],'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isOperator())],
                    ['label' => '<span class="fa fa-eyedropper"></span> Laboratory', 'url' => ['/lab'],'visible' => !Yii::$app->user->isGuest && (Yii::$app->user->identity->isAdmin() || Yii::$app->user->identity->isOperator())],
                    ['label' => '<span class="fa fa-bar-chart"></span> My Reports', 'url' => ['/patients'],'visible' => !Yii::$app->user->isGuest],
                ],
            ]
        );
        ?>
    </section>

</aside>
