<?php

use yii\db\Migration;
use yii\db\Schema;

class m160316_160822_lab_reports extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%reports}}', [
            'id' => Schema::TYPE_PK,
            'date' => Schema::TYPE_INTEGER . ' NOT NULL',
            'report_status'=>Schema::TYPE_INTEGER . ' NOT NULL',
            'patient_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'operator_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%reports}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
