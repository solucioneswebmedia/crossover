<?php

use yii\db\Migration;
use yii\db\Schema;

class m160317_155554_password_reset_token extends Migration
{
    public function up()
    {
        $this->addColumn('users','reset_token',Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('users','reset_token');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
