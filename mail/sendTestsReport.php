<?php
/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>
<table style="width: 800px;">
    <tr style="background-color: #000000;color:#ffffff;">
        <td colspan="4" style="text-align: center;">Test Results Report #<?= $model->id ?></td>
    </tr>
    <tr>
        <td colspan="2"><strong>Patient:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->patient->full_name ?></span></td>
        <td><strong>Taken on:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->getDatetime() ?></span></td>
        <td><strong>Taken by:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->operator->full_name ?></span></td>
    </tr>
    <tr>
        <td><strong>Birth Date:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->patient->birth_date ?></span></td>
        <td><strong>Sex:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->patient->sex==1?'Male':'Female' ?></span></td>
        <td><strong>Mobile Phone:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->patient->mobile_phone ?></span></td>
        <td><strong>Email:</strong><span style="background-color: #000000;color:#ffffff;"><?= $model->patient->email ?></span></td>
    </tr>
    <tr style="background-color: #000000;color:#ffffff;">
        <td colspan="4" style="text-align: center;">&nbsp;</td>
    </tr>
</table>
<p>&nbsp;</p>
<table style="width: 800px;">
    <tr style="background-color: #000000;color:#ffffff;">
        <td colspan="7" style="text-align: center;"><strong>Laboratory Test Results</strong></td>
    </tr>
    <tr style="background-color: #000000;color:#ffffff;">
        <th style="text-align: center;"><strong>#</strong></th>
        <th style="text-align: center;"><strong>Profile</strong></th>
        <th style="text-align: center;"><strong>Test</strong></th>
        <th style="text-align: center;"><strong>Value</strong></th>
        <th style="text-align: center;"><strong>lower value</strong></th>
        <th style="text-align: center;"><strong>upper value</strong></th>
        <th style="text-align: center;"><strong>Units</strong></th>
    </tr>
    <?php foreach ($model->getTestResultsQuery()->all() as $key=>$test) { ?>
        <tr>
            <td style="text-align: center;"><?= $key ?></td>
            <td><?= (isset($test->profile))?$test->profile->description:"" ?></td>
            <td><?= $test->test->name ?></td>
            <td><?= $test->value ?></td>
            <td style="text-align: center;"><?= $test->test->lower_limit ?></td>
            <td style="text-align: center;"><?= $test->test->upper_limit ?></td>
            <td style="text-align: center;"><?= $test->test->units ?></td>
        </tr>
    <?php } ?>
</table>
<hr/>
<p>This Results are confidencial and protected by law. Please delete this message from your inbox if you are not the right recipient of this information. Be carefull with the environment, do not print this results unless if strictly necessary.</p>
<hr/>
<p>Remenber you can see all your test results online on <a target="_blank" href="<?= Yii::$app->urlManager->createAbsoluteUrl("/") ?>"> LISA Web Application.</a></p>
<P> <strong>User:</strong> <?= $model->patient->full_name ?> - <strong>Pass code:</strong> <?= $model->patient->reset_token ?></p> 