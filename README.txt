Installation Process

Prerequisites: you have to manage to setup a local/server environment with this configuration

1) Web Server with PHP Support (recommended apache)

2) PHP5 with cli support (required extensions:gd,xml,curl)

3) Composer 

4) Empty MySQL database


Setup Steps:

1) Copy the application source code folder to your local/server environment

2) go to application folder and run command: composer install

after composer finish install, go to your /application/vendor folder. If there is a /application/vendor/bower-asset folder but no an /application/vendor/bower folder create a symlink using this command: ln -s bower-asset bower 

3) create an empty database on mysql

4) edit /application/config/db.php and put your database connection settings

<?php

	return [
	    'class' => 'yii\db\Connection',
	    'dsn' => 'mysql:host=localhost;dbname=dna',
	    'username' => 'root',
	    'password' => 'root',
	    'charset' => 'utf8',
	];

5) edit /application/config/console.php and edit this line to point to your web application URL

	Yii::setAlias('@lisa', "http://labs.arepas.app");

6) edit /application/config/web.php and put your mail server configuration

	'components' => [
	        'mailer' => [
	            'class'            => 'zyx\phpmailer\Mailer',
	            'viewPath'         => '@common/mail',
	            'useFileTransport' => false,
	            'config'           => [
	                'mailer'     => 'smtp',
	                'host'       => 'smtp.yandex.ru',
	                'port'       => '465',
	                'smtpsecure' => 'ssl',
	                'smtpauth'   => true,
	                'username'   => 'mysmtplogin@example.ru',
	                'password'   => 'mYsmTpPassword',
	            ],
	        ],
	    ],
	];

	also edit the following lines with your outbound email address  

		'messageConfig'    => [
	                'from' => ['noreply@example.com' => 'LISA Systems']
	            ],
7) Run setup command to initialize the empty database. in application folder run: 
	
			./yii setup

	after that you will be prompted to run database migrations, 

		Apply the above migrations? (yes|no) [no]:

	type yes and press enter, your empty mysql database now has all tables and data needed to start to play with the application

8) configure apache vhost with root folder /application/web and enabled rewrite apache module for this directory. Use the following directory apache configuration snippet
        

        DocumentRoot /path/to/application/web
        <Directory /path/to/application/web>
                Options Indexes FollowSymlinks MultiViews
                AllowOverride All
                Require all granted
                # use mod_rewrite for pretty URL support
                RewriteEngine on
                # If a directory or a file exists, use the request directly
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteCond %{REQUEST_FILENAME} !-d
                # Otherwise forward the request to index.php
                RewriteRule . index.php
        </Directory>

9) restart apache and point your browser to your apache's URL and start to playing with the app  go to login page use System Adminitrator user with password: sysadmin

Note: if you experience problems generating PDF  Reports run this on /application folder: chmod -R 777 vendor


ASSUMPTIONS MADE & MISSIN REQUIREMENTS

1) Patients only can view their reports

2) Operator can manage the Patients CRUD and create and fill Test Results Reports

3) An Admin users can manage users and setup laboratory tests and profiles

4) A profile is a set of tests that performs together

5) Admin and Operators users can change their password. Users can not change their password (aka pass code)

6) Some parts have good validation code in  other  parts can be improved


Feedback

Is pretty fun this assigment. However since I'm already working in a project for an old customer I can'n manage to complete unit testing for this application.


Contact Info: If you like what I've done with this assigment go ahead and reach me on

Whatsapp: +57 319.451.7896
Email: jscarton@gmail.com


